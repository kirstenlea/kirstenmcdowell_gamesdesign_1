﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingStairs : MonoBehaviour
{
    public Transform MovingPlatform;

    public Transform startTransform;

    public Transform endTransform;

    public float platformSpeed;

    Vector3 direction;
    Transform destination;

    private void Start()
    {
        setDestination(startTransform);
    }

    
    void FixedUpdate() {
        MovingPlatform.GetComponent<Rigidbody>().MovePosition(MovingPlatform.position + direction * platformSpeed * Time.fixedDeltaTime);

        if (Vector3.Distance(MovingPlatform.position, destination.position) < platformSpeed * Time.fixedDeltaTime){
            setDestination(destination = startTransform ? endTransform : startTransform);
          }
        }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(startTransform.position, MovingPlatform.localScale);

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(endTransform.position, MovingPlatform.localScale);

    }

    void setDestination(Transform dest)
    {
        destination = dest;
        direction = (destination.position - MovingPlatform.position).normalized;
    }
}