﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerBalcony : MonoBehaviour
{

    public Animation anim;


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("4"))
        {
            anim.Play("MovingPlatformBalcony");
            //Invoke("PauseAnim", 1.5f);
        }
        if (Input.GetKeyDown("4"))
        {

            anim.Play("MovingPlatformBalcony");
        }
    }

    void PauseAnim()
    {
        //anim.Rewind();
    }

    public class HoldPlayer : MonoBehaviour
    {

        void OnTriggerStay(Collider col)
        {
            col.transform.parent = gameObject.transform;
        }

        void OnTriggerExit(Collider col)
        {
            col.transform.parent = null;
        }
    }


}
