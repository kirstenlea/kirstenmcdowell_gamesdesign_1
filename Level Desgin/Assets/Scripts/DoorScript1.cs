﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript1 : MonoBehaviour {

    public bool open = false;
    public float doorOpenAngle = 90f;
    public float doorCloseAngle = 0f;
    public float smooth = 2f;

	void Start ()
    {
		
	}
	
	public void ChangeDoorState()
    {
        open = !open;
    }

	void Update ()
    {
		if(open) //open == true
        {
            Quaternion targetRot = Quaternion.Euler(0, doorOpenAngle, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot, smooth * Time.deltaTime);
        }
        else
        {
            Quaternion targetRot2 = Quaternion.Euler(0, doorOpenAngle, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot2, smooth * Time.deltaTime);
        }
	}
}
