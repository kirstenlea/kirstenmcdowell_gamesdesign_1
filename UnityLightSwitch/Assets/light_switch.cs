﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class light_switch : MonoBehaviour
{
    public trigger_lister trigger;
    public Image cursorImage;

    //light variable
    public Light spotlight;
    //audio variable
    public AudioSource switchAudio;
    //animation variable
    Animation anim;

    // Use this for initialization
    void Start()
    {
        switchAudio = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
        cursorImage.enabled = false;
    }

    // called when cursor is over object
    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {

            if(cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
            Debug.Log("Mouse over switch");
        }
        else
        {
            cursorImage.enabled = false;
        }
    }

    private void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        switchAudio.Play();

        if (trigger.playerEntered == true)
        {
            switchAudio.Play();
            anim.Stop();
            anim.Play();

            if(spotlight.intensity > 0)
            {
                spotlight.intensity = 0f;                
            }
            else
            {
                spotlight.intensity = 1.5f;
            }
        }
    }
}
